import React from 'react';
import './App.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faRocket} from "@fortawesome/free-solid-svg-icons";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <div className="rocket-wrapper">
                    <FontAwesomeIcon className="rocket" icon={faRocket} color="#61dafb" size="10x" data-testid="rocket"/>
                </div>
                <p>
                    You did It! <span role="img" aria-label="party">🎉</span>
                </p>
            </header>
          <footer className="App-footer">
            <p>&copy; <a href="https://www.felix-franz.com/" target="_blank" rel="noopener noreferrer">Felix Franz</a> & <a href="http://www.berndneeser.de/" target="_blank" rel="noopener noreferrer">Bernd Neeser</a></p>
          </footer>
        </div>
    );
}

export default App;
